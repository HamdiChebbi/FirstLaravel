@extends('layouts.app')
@section('content')
    <h1>Nice Contact Page</h1>

    @if(count($people))
        <ul>
        @foreach($people as $person)
            <li>{{$person}}</li>
            @endforeach
        </ul>


    @else
        <b>No people here :(</b>
    @endif
    @endsection