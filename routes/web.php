<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

use Illuminate\Support\Facades\DB;
use App\Post;

Route::get('/', function () {
    return view('welcome');
});


Route::get('/about', function () {
    return 'Hi about page';
});

Route::get('contact', 'PostController@contact');

// passer des multiples paramétres
Route::get('/post/{id}/{name}' , function ($id , $name){
    return 'This is post number '. $id. " ". $name ;
});

// une route Nommée
Route::get('admin/post/exemple' , array('as'=>'admin.home', function(){
    $url = route('admin.home') ;
    return "this url is : ". $url ;
}))  ;

//Route::get('post/{id}','PostController@index') ;

// création de tt les URL d'un controlleur
Route::resource('posts','PostController') ;

// route , view with parametres

Route::get('post/{id}/{name}/{password}','PostController@show_post') ;


//Route::get('contact','PostController@contact') ;
//
// route to show the login form
Route::get('login', array('uses' => 'HomeController@showLogin'));

/*
______________________________________
Row Query Route
______________________________________
*/
Route::get('/insert', function (){
    DB::insert('insert into posts(title,content) VALUE (? ,?)',['Post 5','Content post 5 ']);
});

// Read Posts
Route::get('/read',function (){
   $results= DB::select('select * from posts where id=?', [1]);
    return $results;
});

//Update
Route::get('/update',function() {
    $updated = DB::update('update posts set title="Title post updated" WHERE id =? ', [1]);
    return $updated;
});

/*
______________________________________
Elequent Model ORM
______________________________________
*/

// Retreive simple
Route::get('/readP', function (){
//$post = Post::find(1);
    $posts = Post::all();
    foreach ($posts as $post){
        return $post;
    }
   // return $post->title;
});

// Retreive with query ( where )
Route::get('/findwhere', function (){
   $posts = Post::where('id',4)->orderBy('id','DESC')->take(1)->get();
    return $posts;
});

// Retreive with more Data
Route::get('/findmore',function (){
    $posts = Post::findOrFail(60);
    return $posts;
});

//Basic insert
Route::get('/inserbasic',function (){
    $post= new Post;
     $post->title = "Post 7";
    $post->content="Content post 7";

    $post->save();
});
//Insert with config mass assignement
Route::get('/insertmass', function (){
    Post::create([ 'title'=>'Post 8', 'content'=>'Content post 8']);
});

//Update data with Elequent
Route::get('/updateE',function (){
    Post::where('id',2)->where('is_admin',0)->update(['title'=>'Nice 2 updated','content'=>'The second post updated ']);
});

//Delete data with Elequent
Route::get('deleteE', function (){
    Post::destroy([7,8]);
});

// Delete simple
Route::get('/deleteat', function (){
    Post::find(4)->delete();
});
//