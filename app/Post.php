<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    use SoftDeletes;

    protected $dates=['deleted_at']; // une colonne qui nous indique quand (date) le post a été supprimé

    //allow data to b insered in the db
    protected $fillable = [
        'title',
        'content'
    ];

}
